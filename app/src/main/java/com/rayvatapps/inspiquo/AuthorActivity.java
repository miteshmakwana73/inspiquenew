package com.rayvatapps.inspiquo;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rayvatapps.inspiquo.adapter.AuthorAdapter;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.model.Author;
import com.rayvatapps.inspiquo.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AuthorActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;

    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Author> authorList;
    private AuthorAdapter mAdapter;
    private SearchView searchView;
    Toolbar toolbar;

    SwipeRefreshLayout sw_refresh;
    NavigationView navigationView;
    Intent intent;

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";//http://inspiquo.rayvatapps.com/api/api.php?request=";1.0

    // url to fetch contacts json
    String URL = main_url+"getauthors"; //"https://api.androidhive.info/json/contacts.json";

    boolean doubleBackToExitPressedOnce = false;

    TextView quotesstatus;
    Mysql mySql;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);

        quotesstatus=(TextView)findViewById(R.id.tvstatus);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        quotesstatus.setTypeface(tf);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Author");
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share app", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); /*Here's a Snackbar*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.

                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());

                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        authorList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        mAdapter.notifyDataSetChanged();

                        //loadallcategory();
                        checkconnection();

                        mAdapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);
                    }
                }, 1000);

            }
        });
        //getSupportActionBar().setTitle(R.string.toolbar_title);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        authorList = new ArrayList<>();
        mAdapter = new AuthorAdapter(this, authorList);

        // white background notification bar
        //whiteNotificationBar(recyclerView);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new AuthorActivity.GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //adding the adapter to listview
        recyclerView.setAdapter(mAdapter);

        checkconnection();
    }


    private void fetchAuthor() {

        mySql = new Mysql(AuthorActivity.this);

        mySql.deleteAllAuthor();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        authorList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray responsea = obj.getJSONArray("response");

                            for (int i = 0; i < responsea.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = responsea.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                Author hero = new Author(heroObject.getInt("Aid"),
                                        heroObject.getString("Name"),
                                        heroObject.getString("Bio"),
                                        heroObject.getString("Image"));  //covers[i]);


                                mySql.insertQuoteAuthor(
                                        heroObject.getInt("Aid"),
                                        heroObject.getString("Name"),
                                        heroObject.getString("Bio"),
                                        heroObject.getString("Image")
                                );
                                //adding the hero to herolist
                                authorList.add(hero);
                            }
                            List<Author> items = new Gson().fromJson(responsea.toString(), new TypeToken<List<Author>>() {
                            }.getType());

                            // adding contacts to contacts list
                            /*contactList.clear();
                            contactList.addAll(items);*/

                        } catch (JSONException e) {
                            quotesstatus.setText("No Author");
                            progressBar.setVisibility(View.INVISIBLE);
                            quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                  /*  @Override
                    public void onResponse(JSONArray response) {
                        if (response == null) {
                            Toast.makeText(getApplicationContext(), "Couldn't fetch the contacts! Pleas try again.", Toast.LENGTH_LONG).show();
                            return;
                        }*/
                        // refreshing recycler view
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                // error in getting json
                //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();
                if( volleyError instanceof NoConnectionError) {
                    Toast.makeText(AuthorActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                }
                else if (volleyError.getClass().equals(TimeoutError.class)) {
                    // Show timeout error message
                    Toast.makeText(AuthorActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                }
                else if (volleyError instanceof ServerError) {
                    // Show timeout error message
                    Toast.makeText(AuthorActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                }
                else
                {
                    //quotesstatus.setVisibility(View.VISIBLE);
                    Toast.makeText(AuthorActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                }
                checkconnection();
              /*  Log.e("Error: " , String.valueOf(error));
                Toast.makeText(getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();*/
            }
        });

        MyApplication.getInstance().addToRequestQueue(stringRequest);
    }


    private void checkconnection() {
        quotesstatus.setVisibility(View.GONE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            fetchAuthor();

            showBannerAddvertisement();
            //connection is avlilable
        }else{
            //no connection
            //displayAlert();
            //Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show();
            mySql = new Mysql(this);
            authorList = mySql.getAuthor();
            if(authorList.size()==0)
            {
                quotesstatus.setText("No Author");
                quotesstatus.setVisibility(View.VISIBLE);

                Toast.makeText(this, "Please Go Online one time", Toast.LENGTH_SHORT).show();
            }
            mAdapter = new AuthorAdapter(this,authorList);
            recyclerView.setAdapter(mAdapter);
        }
    }

    private void showBannerAddvertisement() {
        AdView mAdMobAdView;

        mAdMobAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        adView.setAdSize(AdSize.BANNER);
        mAdMobAdView.loadAd(adRequest);

        mAdMobAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void displayAlert() {
        new AlertDialog.Builder(this).setMessage("Please Check Your Internet Connection and Try Again")
                .setTitle("Network Error")
                .setCancelable(false)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                finish();
                            }
                        })
                .show();
    }

    @Override
    public void onBackPressed() {

        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_search:
                Toast.makeText(AuthorActivity.this, "Search", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }*/

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.getMenu().getItem(2).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        switch (menuItem.getItemId()) {
                            case R.id.nav_category:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(AuthorActivity.this,CategoriesActivity.class);
                                startActivity(intent);
                                finish();

                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(AuthorActivity.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_author:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(AuthorActivity.this,AuthorActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_favourites:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(AuthorActivity.this,FavouritesActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_rate:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                Uri uri = Uri.parse("market://details?id=" + AuthorActivity.this.getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                // To count with Play market backstack, After pressing back button,
                                // to taken back to our application, we need to add following flags to intent.
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                                }
                                return true;

                            case R.id.nav_about:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(AuthorActivity.this,AboutActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_privacy:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(AuthorActivity.this,PrivacyActivity.class);
                                startActivity(intent);
                                finish();
                                return true;
                        }
                        return false;
                    }
                });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
