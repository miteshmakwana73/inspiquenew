package com.rayvatapps.inspiquo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skyfishjy.library.RippleBackground;

public class SplashScreen extends AppCompatActivity {

    Intent intent;
    View mDecorView;
    RippleBackground rippleBackground;
    ImageView imageView;
    LinearLayout linearLayout;
    int a=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        linearLayout=(LinearLayout)findViewById(R.id.lnsplash);
        rippleBackground=(RippleBackground)findViewById(R.id.content);

//        changecolor(a);
        mDecorView = getWindow().getDecorView();

        hideSystemUI();

        loadanimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                        intent = new Intent(SplashScreen.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                } catch (Exception e) {

                }
            }
        }, 3000);
    }

    private void changecolor(final int b) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if(b==1) {
                        linearLayout.setBackgroundColor(Color.YELLOW);
                        changecolor(b+1);
                    }
                    else {
                        linearLayout.setBackgroundColor(Color.YELLOW);
                        changecolor(b+1);
                    }
                } catch (Exception e) {

                }
            }
        }, 1000);
    }

    private void loadanimation() {
        rippleBackground.startRippleAnimation();
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
