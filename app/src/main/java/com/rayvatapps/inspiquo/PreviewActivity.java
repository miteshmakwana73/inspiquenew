package com.rayvatapps.inspiquo;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.rayvatapps.inspiquo.adapter.QuotesAdapter;
import com.rayvatapps.inspiquo.db.Mysql;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by mitesh on 18/06/18.
 */

public class PreviewActivity extends AppCompatActivity {
    View mDecorView;

    ProgressBar progressBar;
    RelativeLayout lnimg;
    TextView tvquote,tvauthor;
    ImageView imgquote,download,loader;
    LinearLayout lnwallpaper,lndownload;
    RelativeLayout main;
    View viewpreview;
    String quote,author,imgpath;
    int imgtype,count,imgheight;

    Mysql mySql;

    ArrayList<String> imageList;
    final Random rnd = new Random();

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";

    String JSON_IMG_URL = main_url+"getbackgrounds";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        mDecorView = getWindow().getDecorView();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        hideSystemUI();

        imageList = new ArrayList<String>();

        checkconnection();

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            quote="";
            author="";
            imgpath="";
//            imgheight=0;
            imgtype=0;
        } else {
            quote= extras.getString("quote");
            author= extras.getString("author");
            imgpath= extras.getString("imgpath");
//            imgheight= extras.getInt("imgheight");
            imgtype= extras.getInt("type");
            Log.e("Quote "+ quote," author "+author +" type "+imgtype);
        }
        lnimg=(RelativeLayout)findViewById(R.id.lnimg);
        imgquote=(ImageView)findViewById(R.id.imgquote);
        download=(ImageView)findViewById(R.id.imgsave);
        loader=(ImageView)findViewById(R.id.imgloader);
        lndownload=(LinearLayout)findViewById(R.id.lndownload);
        lnwallpaper=(LinearLayout)findViewById(R.id.lnsetwallpaper);
        main=(RelativeLayout) findViewById(R.id.lnmain);
        tvquote=(TextView) findViewById(R.id.quote);
        tvauthor=(TextView) findViewById(R.id.author);
        viewpreview=(View) findViewById(R.id.viewpreview);
        viewpreview.setVisibility(View.VISIBLE);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        tvquote.setTypeface(tf);

        /*byte[] byteArray = getIntent().getByteArrayExtra("image");
//        byte[] byteArray = Base64.decode(getIntent().getByteArrayExtra("image"), Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);*/

//        imgquote.setBackgroundResource(R.drawable.scrim);

        tvquote.setText(quote);
        tvauthor.setText(author);

        if(imgtype==2)
        {
            main.setBackgroundColor(Color.parseColor("#FCFEFC"));
            Glide.with(PreviewActivity.this)
                    .load(R.drawable.download_loader)
                    .into(new GlideDrawableImageViewTarget(loader));
//            imgquote.setImageBitmap(bmp);

            if(imgpath.equals(""))
            {
                Glide.with(this)
                        .load(R.drawable.board)
                        .asBitmap()
                        .error(R.color.black)
                        .into(imgquote);
            }
            else
            {
                Glide.with(this)
                        .load(imgpath)
                        .asBitmap()
                        .error(R.color.black)
                        .into(imgquote);
            }

            imgquote.setScaleType(ImageView.ScaleType.CENTER_CROP);

            tvquote.setTextSize(30);
            tvauthor.setTextSize(20);

            lnwallpaper.setVisibility(View.GONE);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = getBitmapFromLayout();

                    saveImageToExternalStorage(bitmap);
                }
            }, 2000);

            /*Bitmap bitmap = getBitmapFromLayout();

            saveImageToExternalStorage(bitmap);*/
        }
        else if(imgtype==3)
        {

            Glide.with(PreviewActivity.this)
                    .load(R.drawable.time_loader)
                    .into(new GlideDrawableImageViewTarget(loader));

            if(imgpath.equals(""))
            {
                Glide.with(this)
                        .load(R.drawable.board)
                        .asBitmap()
                        .error(R.color.black)
                        .into(imgquote);
            }
            else {
                Glide.with(this)
                        .load(imgpath)
                        .asBitmap()
                        .error(R.color.black)
                        .into(imgquote);
            }
            imgquote.setScaleType(ImageView.ScaleType.CENTER_CROP);

            tvquote.setTextSize(30);
            tvauthor.setTextSize(20);

            lndownload.setVisibility(View.GONE);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                progressBar.setVisibility(View.VISIBLE);
                lnimg.setDrawingCacheEnabled(true);
                lnimg.buildDrawingCache();
                Bitmap bitmap = lnimg.getDrawingCache();

                /*WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                try {
                    myWallpaperManager.setBitmap(bitmap);
                    Toast.makeText(PreviewActivity.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }     */

                try {
                    setWallpaper(bitmap);
                    Toast.makeText(PreviewActivity.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                }
            }, 3000);

        }
        else
        {
            main.setBackgroundColor(Color.parseColor("#FCFEFC"));

            Glide.with(PreviewActivity.this)
                    .load(R.drawable.download_loader)
                    .into(new GlideDrawableImageViewTarget(loader));

            if(imgpath.equals(""))
            {
                Glide.with(this)
                        .load(R.drawable.board)
                        .asBitmap()
                        .fitCenter()
                        .error(R.color.black)
                        .into(imgquote);
            }
            else
            {
                Glide.with(this)
                        .load(imgpath)
                        .asBitmap()
                        .fitCenter()
                        .error(R.color.black)
                        .into(imgquote);
            }
//            viewpreview.setLayoutParams(new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 500));
            ViewGroup.LayoutParams params = lnimg.getLayoutParams();
            params.height = 400;//imgquote.getHeight();//getHeight();//imgheight;
            params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lnimg.setLayoutParams(params);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = getBitmapFromLayout();

                    saveImageToExternalStorage(bitmap);
                }
            }, 2000);
//            imgquote.setScaleType(ImageView.ScaleType.FIT_CENTER);

            /*Bitmap bitmap = getBitmapFromLayout();

            saveImageToExternalStorage(bitmap);*/

        }

        lnwallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                lnimg.setDrawingCacheEnabled(true);
                lnimg.buildDrawingCache();
                Bitmap bitmap = lnimg.getDrawingCache();

                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                try {
                    myWallpaperManager.setBitmap(bitmap);
                    Toast.makeText(PreviewActivity.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        lndownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                lnimg.setDrawingCacheEnabled(true);
//                lnimg.buildDrawingCache();
//                Bitmap bitmap = lnimg.getDrawingCache();
                Bitmap bitmap = getBitmapFromLayout();

                saveImageToExternalStorage(bitmap);


            }
        });

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
//            loadAllQuoteimage();
            imageList=MainActivity.imageList;
        }else{
            //no connection
            mySql = new Mysql(this);
            imageList = mySql.getQuoteImage();
        }
    }

    private void loadAllQuoteimage() {
//        mySql = new Mysql(PreviewActivity.this);

//        mySql.deleteAllQuoteImage();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_IMG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);
                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("response");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                final String data=heroObject.getString("Image");



                                final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                final String fullpath=path+data;

                                Thread thread = new Thread(new Runnable() {

                                    @Override
                                    public void run() {
                                        try  {
                                            try {

                                                //Your code goes here
                                                URL url = new URL(fullpath);
                                                HttpGet httpRequest = null;

                                                httpRequest = new HttpGet(url.toURI());

                                                HttpClient httpclient = new DefaultHttpClient();
                                                HttpResponse response = (HttpResponse) httpclient
                                                        .execute(httpRequest);

                                                HttpEntity entity = response.getEntity();
                                                BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                                                InputStream input = b_entity.getContent();

                                                Bitmap bitmap = BitmapFactory.decodeStream(input);
                                                BitmapDrawable background = new BitmapDrawable(bitmap);

//                                                saveImageToExternalStorage(bitmap,data);

                                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                //compress the image to jpg format
                                                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
                                                /*
                                                 * encode image to base64 so that it can be picked by saveImage.php file
                                                 * */
                                                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);
                                                //Log.e("image",encodeImage);
                                                imageList.add(encodeImage);


//                                                mySql.insertQuoteImage(encodeImage);



                                                //holder.lnimg.setBackgroundDrawable(background);

                                            } catch (URISyntaxException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();

                                            } catch (MalformedURLException e) {
                                                Log.e("log", "bad url");
                                            } catch (IOException e) {
                                                Log.e("log", "io error");
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                                thread.start();

                            }

                        } catch (JSONException e) {

                            //quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(volleyError));
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(PreviewActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(PreviewActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(PreviewActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(PreviewActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        checkconnection();
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private Bitmap getBitmapFromLayout() {

        Bitmap mBitmap = null;
        try {
            if (mBitmap != null) {
                mBitmap.recycle();
            }
            mBitmap = Bitmap.createBitmap(lnimg.getWidth(), lnimg.getHeight(), Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setFilterBitmap(true);
            paint.setAntiAlias(true);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            Canvas canvas = new Canvas(mBitmap);
            lnimg.draw(canvas);
            canvas.drawBitmap(mBitmap, 0, 0, paint);
            return mBitmap;
        } catch (Exception e) {
            e.printStackTrace();
            System.gc();
        }
        return mBitmap;
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(PreviewActivity.this, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                       /* Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);*/
                    }
                });

        Toast.makeText(PreviewActivity.this, "Saved to Gallery", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
