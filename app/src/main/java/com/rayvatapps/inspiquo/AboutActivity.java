/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rayvatapps.inspiquo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;

import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rayvatapps.inspiquo.adapter.PrivacyAdapter;
import com.rayvatapps.inspiquo.adapter.SubCategoryAdapter;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.model.Privacy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 */
public class AboutActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;

    private RecyclerView recyclerView;
    private PrivacyAdapter adapter;
    private List<Privacy> albumList;

    Intent intent;

    SwipeRefreshLayout sw_refresh;

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";

    // private static final String JSON_URL = "http://192.168.1.8:9001/api/api.php?request=getquotes";

    String JSON_URL = main_url+"getsettings";

    ArrayList<String> arryfavquoteid;

    boolean doubleBackToExitPressedOnce = false;
    TextView quotesstatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        arryfavquoteid = new ArrayList<String>();

        quotesstatus=(TextView)findViewById(R.id.tvstatus);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        quotesstatus.setTypeface(tf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("About Us");
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        //setNavigationViewListner();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share app", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); /*Here's a Snackbar*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.

                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter = new PrivacyAdapter(getApplicationContext(),albumList,1);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

       checkconnection();

    }

    private void checkconnection() {
        quotesstatus.setVisibility(View.GONE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            loadAllQuotejsondata();

            showBannerAddvertisement();

            //connection is avlilable
        }else{
            //no connection
            //displayAlert();
            //Toast toast = Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG);toast.show();
            SharedPreferences sharedPreferences = getSharedPreferences("About", 0);
            String about = sharedPreferences.getString("about","");

//            Log.e("abo",about);
            if(about.equals(""))
            {
                quotesstatus.setText("No Aboutus");
                quotesstatus.setVisibility(View.VISIBLE);
                Toast.makeText(this, "Please Go Online one time", Toast.LENGTH_SHORT).show();
            }
            else
            {
                quotesstatus.setVisibility(View.VISIBLE);
                quotesstatus.setText(Html.fromHtml(about));
            }
        }
    }

    private void loadAllQuotejsondata() {

        SharedPreferences sharedPreferences = getSharedPreferences("About", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);


        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array

                            JSONObject object = obj.getJSONObject("response");
                           /* String attr1 = object.getString("Aboutus");
                            Log.e("data",attr1);*/
                            Privacy hero = new Privacy(object.getInt("ID"),
                                    object.getString("Aboutus"),
                                    object.getString("Privacypolicy"));


                            SharedPreferences sharedPreferences = getSharedPreferences("About", 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("about", object.getString("Aboutus"));
                            editor.commit();

                            //Log.e("About",object.getString("Aboutus"));

                            albumList.add(hero);

                            //creating custom adapter object
                            adapter = new PrivacyAdapter(getApplicationContext(),albumList,1);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            quotesstatus.setText("No about us");
                            quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(error));
//                        Toast.makeText(getApplicationContext(), "Connection slow", Toast.LENGTH_SHORT).show();
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(AboutActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(AboutActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(AboutActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            //quotesstatus.setVisibility(View.VISIBLE);
                            Toast.makeText(AboutActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        checkconnection();

                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void showBannerAddvertisement() {
        AdView mAdMobAdView;

        mAdMobAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        adView.setAdSize(AdSize.BANNER);
        mAdMobAdView.loadAd(adRequest);

        mAdMobAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    public void displayAlert() {
        new AlertDialog.Builder(this).setMessage("Please Check Your Internet Connection and Try Again")
                .setTitle("Network Error")
                .setCancelable(false)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                finish();
                            }
                        })
                .show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
//        navigationView.getMenu().get(4).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {

                    case R.id.nav_category:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(AboutActivity.this,CategoriesActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_home:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                         intent = new Intent(AboutActivity.this,MainActivity.class);

                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_author:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(AboutActivity.this,AuthorActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_favourites:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(AboutActivity.this,FavouritesActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_rate:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        Uri uri = Uri.parse("market://details?id=" + AboutActivity.this.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + AboutActivity.this.getPackageName())));
                        }
                        return true;

                    case R.id.nav_about:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        /*intent = new Intent(AboutActivity.this,AboutActivity.class);
                        startActivity(intent);
                        finish();*/
                        return true;

                    case R.id.nav_privacy:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(AboutActivity.this,PrivacyActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                }
                return false;
               /* menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                Toast.makeText(MainActivity.this, "Tghghlkh", Toast.LENGTH_SHORT).show();
                return true;*//**/
            }
        });
    }

}
