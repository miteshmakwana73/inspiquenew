package com.rayvatapps.inspiquo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.rayvatapps.inspiquo.model.Author;
import com.rayvatapps.inspiquo.model.Category;
import com.rayvatapps.inspiquo.model.Quote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitesh on 6/1/18.
 */

public class Mysql extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "QuoteDB";
    private static final int DATABASE_VERSION = 4;
    private Context mycontext;

    public Mysql(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mycontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table tbl_Quote(Quote_id INTEGER PRIMARY KEY AUTOINCREMENT ,Quote_content TEXT,Quote_topic TEXT,Quote_by TEXT,Quote_postedby TEXT,Quote_Image TEXT,Quote_posttime TEXT,Quote_featured TEXT,Quote_active TEXT,Topics_id TEXT,Topics_name TEXT,Topic_description TEXT,Topic_active TEXT,Author_username TEXT)");  //
        db.execSQL("create table tbl_HomeQuote(Quote_id INTEGER PRIMARY KEY AUTOINCREMENT ,Quote_content TEXT,Quote_topic TEXT,Quote_by TEXT,Quote_postedby TEXT,Quote_posttime TEXT,Quote_featured TEXT,Quote_active TEXT,Topics_id TEXT,Topics_name TEXT,Topic_description TEXT,Topic_active TEXT,Author_username TEXT)");
        db.execSQL("create table tbl_SubQuote(Quote_id INTEGER PRIMARY KEY AUTOINCREMENT ,Quote_content TEXT,Quote_topic TEXT,Quote_by TEXT,Quote_postedby TEXT,Category_name TEXT,Quote_posttime TEXT,Quote_featured TEXT,Quote_active TEXT,Topics_id TEXT,Topics_name TEXT,Topic_description TEXT,Topic_active TEXT,Author_username TEXT)");  //
        db.execSQL("create table tbl_QuoteCategory(Category_id INTEGER PRIMARY KEY AUTOINCREMENT ,Category_name TEXT,Category_desc TEXT,Category_image TEXT)");
        db.execSQL("create table tbl_Author(Author_id INTEGER PRIMARY KEY AUTOINCREMENT ,Author_name TEXT,Author_bio TEXT,Author_image TEXT)");
        db.execSQL("create table tbl_SubAuthor(Quote_id INTEGER PRIMARY KEY AUTOINCREMENT ,Quote_content TEXT,Quote_language TEXT,Quote_by TEXT)");  //
        db.execSQL("create table tbl_QuoteImages(Image_id INTEGER PRIMARY KEY AUTOINCREMENT ,Quote_Image TEXT)");  //


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists tbl_Quote");
        db.execSQL("Drop table if exists tbl_HomeQuote");
        db.execSQL("Drop table if exists tbl_SubQuote");
        db.execSQL("Drop table if exists tbl_QuoteCategory");
        db.execSQL("Drop table if exists tbl_Author");
        db.execSQL("Drop table if exists tbl_SubAuthor");
        db.execSQL("Drop table if exists tbl_QuoteImages");
        this.onCreate(db);
    }

    //favourite
    public Cursor getAllFavourites() {
        SQLiteDatabase db = getReadableDatabase();
        String SelectQuery = "Select * from tbl_Quote";
        Cursor cursor = db.rawQuery(SelectQuery, null);

        return cursor;
    }

    public void insertIntoDB(int id, String quote, String quoteby, String imgpath, int favstatus) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Quote_id", id);
        values.put("Quote_content", quote);
        values.put("Quote_by", quoteby);
        values.put("Quote_Image", imgpath);
        values.put("Quote_active", favstatus);

        // 3. insert
        long rowInserted = db.insert("tbl_Quote", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            Toast.makeText(mycontext, "Added to favourites", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));

        }

        // 4. close
        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");

    }

    /* Retrive  data from database */
    public List<Quote> getDataFromDB() {
        List<Quote> modelList = new ArrayList<Quote>();
        String query = "select * from tbl_Quote";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {

                Quote model = new Quote();
                model.setQid(cursor.getInt(cursor.getColumnIndex("Quote_id")));
                model.setQuotecontent(cursor.getString(cursor.getColumnIndex("Quote_content")));
                model.setName(cursor.getString(cursor.getColumnIndex("Quote_by")));
                model.setQuoteimage(cursor.getString(cursor.getColumnIndex("Quote_Image")));

//                Log.e("id "+cursor.getInt(cursor.getColumnIndex("Quote_id"))+" quote "+cursor.getString(cursor.getColumnIndex("Quote_content"))
//                ," By "+cursor.getString(cursor.getColumnIndex("Quote_by")));

                modelList.add(model);
            } while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }

     /*delete a row from database*/

    public void deleteARow(String quoteid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("tbl_Quote", "Quote_id" + " = ?", new String[]{quoteid});
        Toast.makeText(mycontext, "Removed from favourites", Toast.LENGTH_SHORT).show();
        db.close();
    }





    //homequote
    public void insertHomeQuote(int quote_id, String quote_content, String quote_by) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Quote_id", quote_id);
        values.put("Quote_content", quote_content);
        values.put("Quote_by", quote_by);
        //values.put("Quote_active", author_username);

//        Log.e("Q id " + quote_id + " quote " + quote_content, " by " + quote_by);
        // 3. insert
        long rowInserted = db.insert("tbl_HomeQuote", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));

        }

        // 4. close
//        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }

    public List<Quote> getHomequote() {
        List<Quote> modelList = new ArrayList<Quote>();
        String query = "select * from tbl_HomeQuote ORDER BY RANDOM()";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                Quote model = new Quote();
                model.setQid(cursor.getInt(cursor.getColumnIndex("Quote_id")));
                model.setQuotecontent(cursor.getString(cursor.getColumnIndex("Quote_content")));
                model.setName(cursor.getString(cursor.getColumnIndex("Quote_by")));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }
    public void deleteAllHomequote() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_HomeQuote");
        //Toast.makeText(mycontext, "Removed Home Quote", Toast.LENGTH_SHORT).show();
        db.close();
    }





    //category
    public void deleteAllQuoteCategory() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_QuoteCategory");
        //Toast.makeText(mycontext, "Removed Home Quote", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public void insertQuoteCategory(int cid, String title, String description, String image) {
        //Log.d("insert", "before insert");
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        image="http://inspiquo.rayvatapps.com/web/assets/images/category/thumbs/"+image;
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Category_id", cid);
        values.put("Category_name", title);
        values.put("Category_desc", description);
        values.put("Category_image", image);

//        Log.e("C id "+ cid+" Category "+title," Description "+description+" image "+image);
        // 3. insert
        long rowInserted =  db.insert("tbl_QuoteCategory", null, values);
        if (rowInserted != -1)
        {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        }
        else
        {
//            Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));
        }

        // 4. close
        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }


    public List<Category> getCategory() {
        List<Category> modelList = new ArrayList<Category>();
        String query = "select * from tbl_QuoteCategory";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                Category model = new Category();
                model.setCid(cursor.getInt(cursor.getColumnIndex("Category_id")));
                model.setTitle(cursor.getString(cursor.getColumnIndex("Category_name")));
                model.setDescription(cursor.getString(cursor.getColumnIndex("Category_desc")));
                model.setImage(cursor.getString(cursor.getColumnIndex("Category_image")));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());
        return modelList;
    }





    //sub category
    public void deleteAllSubCategoryquote(String categoryname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_SubQuote where Category_name='" + categoryname + "'");
        //Toast.makeText(mycontext, "Removed Sub Category Quote", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public void insertSubQuote(int qid, String quotecontent,  String title, String name) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Quote_id", qid);
        values.put("Quote_content", quotecontent);
        values.put("Quote_by", name);
        values.put("Category_name", title);

//        Log.e("Q id " + qid + " quote " + quotecontent, " by " + name + " Category " + title);
        // 3. insert
        long rowInserted = db.insert("tbl_SubQuote", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));
        }

        // 4. close
//        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }

    public List<Quote> getSubquotebyid(String categoryname) {
        List<Quote> modelList = new ArrayList<Quote>();
        String query = "select * from tbl_SubQuote where Category_name='"+categoryname+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                Quote model = new Quote();
                model.setQid(cursor.getInt(cursor.getColumnIndex("Quote_id")));
                model.setQuotecontent(cursor.getString(cursor.getColumnIndex("Quote_content")));
                model.setName(cursor.getString(cursor.getColumnIndex("Quote_by")));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }



    //author
    public void deleteAllAuthor() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_Author");
        //Toast.makeText(mycontext, "Removed Author", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public List<Author> getAuthor() {
        List<Author> modelList = new ArrayList<Author>();
        String query = "select * from tbl_Author";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                Author model = new Author();
                model.setAid(cursor.getInt(cursor.getColumnIndex("Author_id")));
                model.setName(cursor.getString(cursor.getColumnIndex("Author_name")));
                model.setBio(cursor.getString(cursor.getColumnIndex("Author_bio")));
                model.setImage(cursor.getString(cursor.getColumnIndex("Author_image")));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());
        return modelList;
    }

    public void insertQuoteAuthor(int aid, String name, String bio, String image) {
        //Log.d("insert", "before insert");
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        image="http://inspiquo.rayvatapps.com/web/assets/images/category/thumbs/"+image;
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Author_id", aid);
        values.put("Author_name", name);
        values.put("Author_bio", bio);
        values.put("Author_image", image);

//        Log.e("A id "+ aid+" Author "+name," bio "+bio+" image "+image);
        // 3. insert
        long rowInserted =  db.insert("tbl_Author", null, values);
        if (rowInserted != -1)
        {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        }
        else
        {
//            Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));
        }

        // 4. close
        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }




    //sub author
    public void deleteAllSubAuthor(String categoryname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_SubAuthor where Quote_by='" + categoryname + "'");
        //Toast.makeText(mycontext, "Removed Sub Category Quote", Toast.LENGTH_SHORT).show();
        db.close();
    }
    public void insertSubAuthor(int qid, String quotecontent,  String lang, String name) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Quote_id", qid);
        values.put("Quote_content", quotecontent);
        values.put("Quote_language", lang);
        values.put("Quote_by", name);

//        Log.e("Q id " + qid + " quote " + quotecontent, " by " + name + " Language " + lang);
        // 3. insert
        long rowInserted = db.insert("tbl_SubAuthor", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));
        }

        // 4. close
        db.close();
        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }

    public List<Quote> getSubauthorbyid(String categoryname) {
        List<Quote> modelList = new ArrayList<Quote>();
        String query = "select * from tbl_SubAuthor where Quote_by='"+categoryname+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                Quote model = new Quote();
                model.setQid(cursor.getInt(cursor.getColumnIndex("Quote_id")));
                model.setQuotecontent(cursor.getString(cursor.getColumnIndex("Quote_content")));
                model.setName(cursor.getString(cursor.getColumnIndex("Quote_by")));

                modelList.add(model);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }





    //images
    public void insertQuoteImage(String image) {
        //Log.d("insert", "before insert");
        int status;
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("Quote_Image", image);

//        Log.e("Q id " + quote_id + " quote " + quote_content, " by " + quote_by);
        // 3. insert
        long rowInserted = db.insert("tbl_QuoteImages", null, values);
        if (rowInserted != -1) {
              /*Toast.makeText(mycontext, "New row added, row id: " + rowInserted, Toast.LENGTH_SHORT).show(); */
            //Toast.makeText(mycontext, "Added", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(mycontext, "Something wrong", Toast.LENGTH_SHORT).show();
            //deleteARow(String.valueOf(id));

        }

        // 4. close
        //db.close();
//        Toast.makeText(mycontext, "insert value", Toast.LENGTH_LONG);
//        Log.i("insert into DB", "After insert");
    }

    public ArrayList<String> getQuoteImage() {
        ArrayList<String> modelList = new ArrayList<String>();
        String query = "select * from tbl_QuoteImages ORDER BY RANDOM()";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                String img=cursor.getString(cursor.getColumnIndex("Quote_Image"));

                modelList.add(img);
            }while (cursor.moveToNext());
        }

        //Log.d("Quote data", modelList.toString());

        return modelList;
    }
    public void deleteAllQuoteImage() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from tbl_QuoteImages");
        //Toast.makeText(mycontext, "Removed Home Quote", Toast.LENGTH_SHORT).show();
        db.close();
    }
}

