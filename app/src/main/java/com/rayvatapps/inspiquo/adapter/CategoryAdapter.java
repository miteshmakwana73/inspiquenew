package com.rayvatapps.inspiquo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rayvatapps.inspiquo.R;
import com.rayvatapps.inspiquo.SubCategory;
import com.rayvatapps.inspiquo.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitesh on 16/12/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Category> contactList;
    private List<Category> contactListFiltered;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView)view.findViewById(R.id.cattitle);
            thumbnail = (ImageView)view.findViewById(R.id.thumbnail);

        }
    }

    public CategoryAdapter(Context context, List<Category> contactList) {
        this.context = context;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Typeface tf;
        /*tf = Typeface.createFromAsset(context.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.name.setTypeface(tf);*/

        final Category contact = contactListFiltered.get(position);
        holder.name.setText(contact.getTitle());

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("id", String.valueOf(contact.getCid()));

                Intent intent = new Intent(view.getContext(),SubCategory.class);
                intent.putExtra("position", contact.getCid());
                intent.putExtra("catname", contact.getTitle());
                view.getContext().startActivity(intent);
                //Toast.makeText(mContext, "click....", Toast.LENGTH_SHORT).show();
            }
        });
        Glide.with(context)
                .load("http://inspiquo.rayvatapps.com/web/assets/images/category/thumbs/"+contact.getImage())
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Category> filteredList = new ArrayList<>();
                    for (Category row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }
                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Category>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
