package com.rayvatapps.inspiquo.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.R;
import com.rayvatapps.inspiquo.model.Privacy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 18/12/17.
 */
public class PrivacyAdapter extends RecyclerView.Adapter<PrivacyAdapter.MyViewHolder> {

    private Context mContext;
    private List<Privacy> albumList;
    private List lstfavquoteid;

    Cursor cursor;
    Mysql mySql;
    String quote_id;
    ArrayList<Integer> arryfavquoteid ;

    int status;
    Typeface tf;

    public PrivacyAdapter(Context mContext, List<Privacy> albumList, int i) {
        this.mContext = mContext;
        this.albumList = albumList;
        status=i;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        LinearLayout about,privacy;

        public MyViewHolder(View view) {
            super(view);

            about=(LinearLayout)view.findViewById(R.id.lnquote);
            privacy=(LinearLayout)view.findViewById(R.id.lnquote2);
            title = (TextView) view.findViewById(R.id.txtabout);
            count = (TextView) view.findViewById(R.id.txtprivacy);
        }
    }

    public PrivacyAdapter(Context mContext, List<Privacy> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
    public PrivacyAdapter(Context mContext, List<Privacy> albumList, ArrayList<String> arryfavquoteid) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.lstfavquoteid=arryfavquoteid;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.privacy_single, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        mySql = new Mysql(mContext);
        arryfavquoteid = new ArrayList<Integer>();

        final Privacy album = albumList.get(position);

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);
        holder.count.setTypeface(tf);

        holder.count.setMovementMethod(LinkMovementMethod.getInstance());
        holder.title.setMovementMethod(LinkMovementMethod.getInstance());

        if(status==1)
        {
            holder.about.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.privacy.setVisibility(View.VISIBLE);
        }
        holder.title.setText(Html.fromHtml(album.getAboutus()));
        holder.count.setText(Html.fromHtml(album.getPrivacypolicy()));
    }
 

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}