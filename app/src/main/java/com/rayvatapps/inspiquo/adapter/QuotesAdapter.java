package com.rayvatapps.inspiquo.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.rayvatapps.inspiquo.MainActivity;
import com.rayvatapps.inspiquo.PreviewActivity;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.R;
import com.rayvatapps.inspiquo.model.Quote;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitesh Makwana on 18/05/16.
 */
public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.MyViewHolder> {

    private Context mContext;
    private List<Quote> albumList;
    private List lstfavquoteid;

    Cursor cursor;

    Mysql mySql;
    String imgpath="";
    ArrayList<Integer> arryfavquoteid;

    Typeface tf;
    final Random rnd = new Random();
    int[] image={R.drawable.inspiquo_0,R.drawable.inspiquo_1,R.drawable.inspiquo_2,R.drawable.inspiquo_3,R.drawable.inspiquo_4,
            R.drawable.inspiquo_5,R.drawable.inspiquo_6,R.drawable.inspiquo_7,R.drawable.inspiquo_8};
    ArrayList<String> imageserver,imageList;
    int count,index;

    int status=0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count,qid,copy,share,download,tvquote,tvauthor,tvcopy,tvshare;
        public ImageView thumbnail, overflow,favourite,quotefavourite;
        public LinearLayout lncopy,lnshare,lnfav,lndownload,lnimg,lnimgshadow;
        LinearLayout lnmainImageQuote,lnquotecopy,lnquoteshare,lnquotefav;
        public RelativeLayout rlmainQuote;
        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.quote);
            count = (TextView) view.findViewById(R.id.author);
            copy = (TextView) view.findViewById(R.id.txtcopy);
            share = (TextView) view.findViewById(R.id.txtshare);
            download = (TextView) view.findViewById(R.id.txtsave);
            tvcopy = (TextView) view.findViewById(R.id.tvquotecopy);
            tvshare = (TextView) view.findViewById(R.id.tvquoteshare);
            tvquote = (TextView) view.findViewById(R.id.quotetext);
            tvauthor = (TextView) view.findViewById(R.id.authortext);
            thumbnail = (ImageView) view.findViewById(R.id.imgcopy);
            overflow = (ImageView) view.findViewById(R.id.imgshare);
            favourite = (ImageView) view.findViewById(R.id.imgfav);
            lncopy = (LinearLayout) view.findViewById(R.id.lncopy);
            lnshare = (LinearLayout) view.findViewById(R.id.lnshare);
            lnfav = (LinearLayout) view.findViewById(R.id.lnfav);
            lndownload = (LinearLayout) view.findViewById(R.id.lnsave);
            lnimg=(LinearLayout)view.findViewById(R.id.lnimg);
            lnimgshadow=(LinearLayout)view.findViewById(R.id.lnquote);
            lnmainImageQuote=(LinearLayout)view.findViewById(R.id.lnmainquote);
            rlmainQuote=(RelativeLayout) view.findViewById(R.id.rlmainimg);
            lnquotecopy = (LinearLayout) view.findViewById(R.id.lnquotecopy);
            lnquoteshare = (LinearLayout) view.findViewById(R.id.lnquoteshare);
            lnquotefav = (LinearLayout) view.findViewById(R.id.lnquotefav);
            quotefavourite = (ImageView) view.findViewById(R.id.imgquotefav);
        }
    }

    public QuotesAdapter(Context mContext, List<Quote> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
    public QuotesAdapter(Context mContext, List<Quote> albumList, ArrayList<String> imageList) {
        this.mContext = mContext;
        this.albumList = albumList;
        //this.lstfavquoteid=arryfavquoteid;
        this.imageList=imageList;

    }

    public QuotesAdapter(MainActivity mainActivity, List<Quote> albumList, ArrayList<String> imageList, int i) {
        this.mContext = mContext;
        this.albumList = albumList;
        //this.lstfavquoteid=arryfavquoteid;
        this.imageList=imageList;
        status=i;
    }

    /*public QuotesAdapter(Context mContext, List<Quote> albumList, ArrayList<String> imageList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.imageList=imageList;
    }*/
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardciew_single, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        mySql = new Mysql(mContext);
        arryfavquoteid = new ArrayList<Integer>();
        imageserver = new ArrayList<String>();
        //imageList = new ArrayList<String>();
//        FetchImages();

        //imageserver=imageList;
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);
        holder.copy.setTypeface(tf);
        holder.share.setTypeface(tf);
        holder.tvcopy.setTypeface(tf);
        holder.tvshare.setTypeface(tf);
        holder.download.setTypeface(tf);

        final Quote album = albumList.get(position);
//        holder.title.setText(album.getQuotecontent());
//        holder.count.setText(" - "+album.getName() );

        int length=album.getQuotecontent().length();
        if(length>125){
            holder.lnmainImageQuote.setVisibility(View.VISIBLE);
            holder.rlmainQuote.setVisibility(View.GONE);
        }else {
            holder.lnmainImageQuote.setVisibility(View.GONE);
            holder.rlmainQuote.setVisibility(View.VISIBLE);
        }

        holder.title.setText(Html.fromHtml(album.getQuotecontent()));
        holder.count.setText(" - "+Html.fromHtml(album.getName()) );
        holder.tvquote.setText(Html.fromHtml(album.getQuotecontent()));
        holder.tvauthor.setText(" - "+Html.fromHtml(album.getName()) );

        cursor =mySql.getAllFavourites();

        if (cursor.moveToFirst()) {
            do {
                arryfavquoteid.add(cursor.getInt(cursor.getColumnIndex("Quote_id")));

            } while (cursor.moveToNext());
        }

        if(arryfavquoteid.contains(album.getQid()))
        {
            holder.favourite.setBackgroundResource(R.drawable.ic_star_black_48dp);
            holder.quotefavourite.setBackgroundResource(R.drawable.ic_star_black_48dp);
        }
        else
        {
            holder.favourite.setBackgroundResource(R.drawable.ic_star_border_black_48dp);
            holder.quotefavourite.setBackgroundResource(R.drawable.ic_star_border_black_48dp);
        }

        //loadimage(holder);

       /* if(count==0)
        {
            loadimage(holder);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    count = 1;
                    return;
                }
            }, 2000);

        }*/


        holder.lncopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(holder.title.getText());
                Toast.makeText(mContext, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        holder.lnquotecopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(holder.tvquote.getText());
                Toast.makeText(mContext, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });


        holder.lnshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //showPopup(view);
                PopupMenu popup = new PopupMenu(mContext,view);
                popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        int id = menuItem.getItemId();

                        //noinspection SimplifiableIfStatement

                        if (id == R.id.shtext) {

                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            sharingIntent.setType("text/plain");
                            String shareBody = holder.title.getText().toString();
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Quote");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                            return true;
                        }

                        if (id == R.id.shimage) {
                            holder.lnimg.setDrawingCacheEnabled(true);
                            holder.lnimg.buildDrawingCache();
                            Bitmap bitmap = holder.lnimg.getDrawingCache();

                            String bitmapPath = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap,"title", null);
                            Uri bitmapUri = Uri.parse(bitmapPath);

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            intent.setType("image/png");
                            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Quote");
                            intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
                            intent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + mContext.getPackageName());
                            mContext.startActivity(Intent.createChooser(intent, "Share via"));
                            return true;
                        }
//                        Toast.makeText(mContext, " click"+menuItem.getItemId(), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                });
                popup.show();
                notifyDataSetChanged();
            }
        });

        holder.lnquoteshare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        PopupMenu popup = new PopupMenu(mContext,view);
                        popup.getMenu().add("Text");
//                        popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {

                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                sharingIntent.setType("text/plain");
                                String shareBody = holder.title.getText().toString();
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Quote");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
//                                Toast.makeText(mContext, " click"+menuItem.getItemId(), Toast.LENGTH_SHORT).show();
                                return true;
                            }
                        });
                        popup.show();
                        notifyDataSetChanged();
                    }
                });

        holder.lnfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(arryfavquoteid.contains(album.getQid()))
                {
                    holder.favourite.setBackgroundResource(R.drawable.ic_star_border_black_48dp);
                    mySql.deleteARow(String.valueOf(album.getQid()));
                    Toast.makeText(mContext, "Removed from favourites", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    String tempimage=imgpath;
                    if ( holder.lnimg.getBackground().getConstantState()==mContext.getResources().getDrawable(R.drawable.board).getConstantState())
                    {
                        tempimage="";
                    }
                    holder.favourite.setBackgroundResource(R.drawable.ic_star_black_48dp);
                    int favstatus=1;
                    mySql.insertIntoDB(
                            album.getQid(),
                            album.getQuotecontent(),
                            album.getName(),
                            tempimage,
                            favstatus
                    );
                }

               /* Intent intent = getIntent();
                mContext.startActivity(getIntent());
                mContext.startActivity(new Intent(view.getContext(), CategoriesActivity.class)); */
                notifyDataSetChanged();
            }
        });

        holder.lnquotefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(arryfavquoteid.contains(album.getQid()))
                {
                    holder.quotefavourite.setBackgroundResource(R.drawable.ic_star_border_black_48dp);
                    mySql.deleteARow(String.valueOf(album.getQid()));
                    Toast.makeText(mContext, "Removed from favourites", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    holder.quotefavourite.setBackgroundResource(R.drawable.ic_star_black_48dp);
                    int favstatus=1;
                    mySql.insertIntoDB(
                            album.getQid(),
                            album.getQuotecontent(),
                            album.getName(),
                            "", //passing null
                            favstatus
                    );
                }

               /* Intent intent = getIntent();
                mContext.startActivity(getIntent());
                mContext.startActivity(new Intent(view.getContext(), CategoriesActivity.class)); */
                notifyDataSetChanged();
            }
        });

        holder.lndownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                /*holder.lnimg.setDrawingCacheEnabled(true);
                holder.lnimg.buildDrawingCache();
                Bitmap bitmap = holder.lnimg.getDrawingCache();

                saveImageToExternalStorage(bitmap);*/

                PopupMenu popup = new PopupMenu(mContext,view,R.style.Widget_AppCompat_Light_PopupMenu);
                popup.getMenuInflater().inflate(R.menu.poupup_download, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        int id = menuItem.getItemId();

                        //noinspection SimplifiableIfStatement

                        if (id == R.id.dldownload) {

                            showdownloaddialog(holder);

                           /* holder.lnimg.setDrawingCacheEnabled(true);
                            holder.lnimg.buildDrawingCache();
                            Bitmap bitmap = holder.lnimg.getDrawingCache();

                            saveImageToExternalStorage(bitmap);*/
                            return true;
                        }

                        if (id == R.id.dlwallpaper) {
                            Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;
                            int height = size.y;

                            Log.e("image", String.valueOf(holder.lnimg.getBackground()));
                            String tempimage=imgpath;
                            if ( holder.lnimg.getBackground().getConstantState()==mContext.getResources().getDrawable(R.drawable.board).getConstantState())
                            {
                                tempimage="";
                            }

                            holder.title.setVisibility(View.GONE);
                            holder.count.setVisibility(View.GONE);

                            holder.lnimg.setDrawingCacheEnabled(true);
                            holder.lnimg.buildDrawingCache();
                            Bitmap bitmap = holder.lnimg.getDrawingCache();

                            holder.title.setVisibility(View.VISIBLE);
                            holder.count.setVisibility(View.VISIBLE);
                            bitmap=getResizedBitmap(bitmap,height,width);

                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();

                            Intent in1 = new Intent(mContext, PreviewActivity.class);
//                            in1.putExtra("image",byteArray);
                            in1.putExtra("imgpath",tempimage);
                            in1.putExtra("quote",holder.title.getText().toString().trim());
                            in1.putExtra("author",holder.count.getText().toString().trim());
                            in1.putExtra("type",3);
                            mContext.startActivity(in1);

                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
                notifyDataSetChanged();


            }
        });

        holder.lnimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageList.size()>1)
                {
                    int random = 0;
                    random= rnd.nextInt(imageList.size());
                    File imgFile = new  File(imageList.get(random));

                    if(imgFile.exists()){
                        /*Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        BitmapDrawable background = new BitmapDrawable(mContext.getResources(), myBitmap);
                        holder.lnimg.setBackgroundDrawable(background);*/
                        imgpath=imgFile.getAbsolutePath();

                        Glide.with(mContext).load(imgpath).asBitmap().into(new SimpleTarget<Bitmap>(holder.lnimg.getWidth(), holder.lnimg.getHeight()) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                Drawable drawable = new BitmapDrawable(mContext.getResources(), resource);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    holder.lnimg.setBackground(drawable);
                                }
                            }
                        });
                    }
                }
                else
                {
                }

            }
        });
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(mContext, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.poupup_menu, popup.getMenu());
        popup.show();
    }

    public void showdownloaddialog(final MyViewHolder holder) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);//, R.style.myDialog
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.popupdialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        TextView heading=(TextView)dialogView .findViewById(R.id.tvname);
        TextView tvwallpaper=(TextView)dialogView .findViewById(R.id.tvportrait);
        TextView tvlandscape=(TextView) dialogView .findViewById(R.id.tvlandscape);
        ImageView landscap=(ImageView)dialogView.findViewById(R.id.imglandscape);
        final ImageView portrait=(ImageView)dialogView.findViewById(R.id.imgportrait);
        Button close=(Button) dialogView .findViewById(R.id.button_close);

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        heading.setTypeface(tf);
        tvwallpaper.setTypeface(tf);
        tvlandscape.setTypeface(tf);

        holder.title.setVisibility(View.GONE);
        holder.count.setVisibility(View.GONE);
        holder.lnimg.setDrawingCacheEnabled(true);
        holder.lnimg.buildDrawingCache();
        Bitmap bitmap = holder.lnimg.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        holder.title.setVisibility(View.VISIBLE);
        holder.count.setVisibility(View.VISIBLE);
        portrait.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Glide.with(mContext)
                .load(byteArray)
                .asBitmap()
                .error(R.color.black)
                .into(portrait);

        Glide.with(mContext)
                .load(byteArray)
                .asBitmap()
                .error(R.color.black)
                .into(landscap);



        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();
            }
        });

        landscap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();

                holder.title.setVisibility(View.GONE);
                holder.count.setVisibility(View.GONE);

                holder.lnimg.setDrawingCacheEnabled(true);
                holder.lnimg.buildDrawingCache();
                Bitmap bitmap = holder.lnimg.getDrawingCache();

                holder.title.setVisibility(View.VISIBLE);
                holder.count.setVisibility(View.VISIBLE);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                String tempimage=imgpath;
                if ( holder.lnimg.getBackground().getConstantState()==mContext.getResources().getDrawable(R.drawable.board).getConstantState())
                {
                    tempimage="";
                }


                Intent in1 = new Intent(mContext, PreviewActivity.class);
//                in1.putExtra("image",byteArray);
                in1.putExtra("imgpath",tempimage);
//                in1.putExtra("imgheight",holder.lnimg.getHeight());
                in1.putExtra("quote",holder.title.getText().toString().trim());
                in1.putExtra("author",holder.count.getText().toString().trim());
                in1.putExtra("type",1);
                mContext.startActivity(in1);
            }
        });

        portrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.hide();

                Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                int height = size.y;

                holder.title.setVisibility(View.GONE);
                holder.count.setVisibility(View.GONE);

                holder.lnimg.setDrawingCacheEnabled(true);
                holder.lnimg.buildDrawingCache();
                Bitmap bitmap = holder.lnimg.getDrawingCache();

               /* Drawable drawable=portrait.getDrawable();
                BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
                Bitmap bitmap = bitmapDrawable .getBitmap();
                bitmap=getResizedBitmap(bitmap,height,width);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] imageInByte = stream.toByteArray();*/
//                ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);

                holder.title.setVisibility(View.VISIBLE);
                holder.count.setVisibility(View.VISIBLE);
                bitmap=getResizedBitmap(bitmap,height,width);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                String tempimage=imgpath;

                if ( holder.lnimg.getBackground().getConstantState()==mContext.getResources().getDrawable(R.drawable.board).getConstantState())
                {
                    tempimage="";
                }

                Intent in1 = new Intent(mContext, PreviewActivity.class);
//                in1.putExtra("image",byteArray);
                in1.putExtra("imgpath",tempimage);
                in1.putExtra("quote",holder.title.getText().toString().trim());
                in1.putExtra("author",holder.count.getText().toString().trim());
                in1.putExtra("type",2);
                mContext.startActivity(in1);
            }
        });


    }

    private void loadimage(final MyViewHolder holder) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                try {
                    int a=rnd.nextInt(imageList.size());
                    Bitmap decodedByte = null;
                    BitmapDrawable backgroundoffline = null;
                    byte[] decodedString = Base64.decode(imageList.get(a), Base64.DEFAULT);
                    decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    backgroundoffline = new BitmapDrawable(mContext.getResources(), decodedByte);
                    holder.lnimg.setBackgroundDrawable(backgroundoffline);
                }
                catch (Exception e)
                {
                    loadimage(holder);
                }

            }
        }, 2000);
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                       /* Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);*/
                    }
                });

        Toast.makeText(mContext, "Saved to Gallery", Toast.LENGTH_SHORT).show();


    }

    private ArrayList<String> FetchImages() {
        imageserver.clear();
       /* String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Backgrounds");*/

        ArrayList<String> filenames = new ArrayList<String>();
        String path = Environment.getExternalStorageDirectory()
                + File.separator + "Inspiquo/.Inspiquo Backgrounds";
        Log.e("path ",path);

        File directory = new File(path);
        File[] files = directory.listFiles();

        for (int i = 0; i < files.length; i++)
        {

            String file_name = files[i].getName();
            // you can store name to arraylist and use it later
            filenames.add(file_name);
            //imageserver.add(path+file_name);
        }
        for (String member : filenames){
            Log.i("fetch Member name: ", path+member);
        }
        return filenames;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();

        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

        // CREATE A MATRIX FOR THE MANIPULATION

        Matrix matrix = new Matrix();

        // RESIZE THE BIT MAP

        matrix.postScale(scaleWidth, scaleHeight);

        // RECREATE THE NEW BITMAP

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }
    @Override
    public int getItemCount() {
        return albumList.size();
    }
}