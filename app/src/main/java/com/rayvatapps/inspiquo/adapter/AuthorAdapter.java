package com.rayvatapps.inspiquo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rayvatapps.inspiquo.R;
import com.rayvatapps.inspiquo.SubAuthorActivity;
import com.rayvatapps.inspiquo.SubCategory;
import com.rayvatapps.inspiquo.model.Author;
import com.rayvatapps.inspiquo.model.Category;
import com.rayvatapps.inspiquo.model.Privacy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitesh on 20/01/18.
 */

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Author> contactList;
    private List<Author> contactListFiltered;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,bio;
        public ImageView thumbnail;
        public RelativeLayout rlauthor;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView)view.findViewById(R.id.authortitle);
            rlauthor = (RelativeLayout) view.findViewById(R.id.rlauthor);
            //bio = (TextView)view.findViewById(R.id.authorbio);
            thumbnail = (ImageView)view.findViewById(R.id.thumbnail);

        }
    }

    public AuthorAdapter(Context context, List<Author> contactList) {
        this.context = context;
        this.contactList = contactList;
        this.contactListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.author_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Typeface tf;
        /*tf = Typeface.createFromAsset(context.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.name.setTypeface(tf);*/

        final Author contact = contactListFiltered.get(position);
        holder.name.setText(contact.getName());
        //holder.bio.setText("Bio "+contact.getBio());

        holder.rlauthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),SubAuthorActivity.class);
                intent.putExtra("position", contact.getAid());
                intent.putExtra("catname", contact.getName());
                view.getContext().startActivity(intent);
            }
        });

        Glide.with(context)
                .load("http://inspiquo.rayvatapps.com/web/assets/images/author/thumbs/"+contact.getImage())
                .error(R.drawable.ic_author)
                .into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return contactListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    contactListFiltered = contactList;
                } else {
                    List<Author> filteredList = new ArrayList<>();
                    for (Author row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }
                    contactListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = contactListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                contactListFiltered = (ArrayList<Author>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
