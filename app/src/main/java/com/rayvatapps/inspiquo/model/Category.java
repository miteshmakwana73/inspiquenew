package com.rayvatapps.inspiquo.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Category {

    private int Cid;
    private String Title;
    private String Description;
    private String Image;

    public Category(int cid, String title, String description, String image) {
        Cid = cid;
        Title = title;
        Description = description;
        Image = image;
    }

    public Category() {

    }

    public int getCid() {
        return Cid;
    }

    public void setCid(int cid) {
        Cid = cid;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}