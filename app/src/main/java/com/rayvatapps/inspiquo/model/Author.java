package com.rayvatapps.inspiquo.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Author {

    private int Aid;
    private String Name;
    private String Bio;
    private String Image;

    public Author(int aid, String name, String bio, String image) {
        Aid = aid;
        Name = name;
        Bio = bio;
        Image = image;
    }

    public Author() {

    }

    public int getAid() {
        return Aid;
    }

    public void setAid(int aid) {
        Aid = aid;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getBio() {
        return Bio;
    }

    public void setBio(String bio) {
        Bio = bio;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}