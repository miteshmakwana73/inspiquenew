package com.rayvatapps.inspiquo.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Privacy {

    private int id;
    private String Aboutus;
    private String Privacypolicy;

    public Privacy(int id, String aboutus, String privacypolicy) {
        this.id = id;
        Aboutus = aboutus;
        Privacypolicy = privacypolicy;
    }

    public Privacy() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAboutus() {
        return Aboutus;
    }

    public void setAboutus(String aboutus) {
        Aboutus = aboutus;
    }

    public String getPrivacypolicy() {
        return Privacypolicy;
    }

    public void setPrivacypolicy(String privacypolicy) {
        Privacypolicy = privacypolicy;
    }
}