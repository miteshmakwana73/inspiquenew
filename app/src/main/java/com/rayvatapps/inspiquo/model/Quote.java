package com.rayvatapps.inspiquo.model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Quote {

    private int Qid;
    private String Quotecontent;
    private String Lang;
    private String Title;
    private String Name;
    private String Username;

    private String Quoteimage;


    public Quote(int qid, String quotecontent, String lang, String title, String name, String username) {
        Qid = qid;
        Quotecontent = quotecontent;
        Lang = lang;
        Title = title;
        Name = name;
        Username = username;
    }

    public Quote() {

    }

    public Quote(int qid, String quotecontent, String lang, String name) {
        Qid = qid;
        Quotecontent = quotecontent;
        Lang = lang;
        Name = name;
    }

    public Quote(int qid, String quotecontent, String lang, String name, String username) {
        Qid = qid;
        Quotecontent = quotecontent;
        Lang = lang;
        Name = name;
        Username = username;
    }

    public Quote(String quoteimage) {
        Quoteimage=quoteimage;
    }

    public int getQid() {
        return Qid;
    }

    public void setQid(int qid) {
        Qid = qid;
    }

    public String getQuotecontent() {
        return Quotecontent;
    }

    public void setQuotecontent(String quotecontent) {
        Quotecontent = quotecontent;
    }

    public String getLang() {
        return Lang;
    }

    public void setLang(String lang) {
        Lang = lang;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getQuoteimage() {
        return Quoteimage;
    }

    public void setQuoteimage(String quoteimage) {
        Quoteimage = quoteimage;
    }
}