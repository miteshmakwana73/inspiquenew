package com.rayvatapps.inspiquo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rayvatapps.inspiquo.adapter.QuotesAdapter;
import com.rayvatapps.inspiquo.adapter.SubCategoryAdapter;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.model.Quote;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SubCategory extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SubCategoryAdapter adapter;
    private List<Quote> albumList;

    SwipeRefreshLayout sw_refresh;

    // private static final String JSON_URL = "http://192.168.1.8:9001/api/api.php?request=getquotes";

    Bundle bundle;
    int  newCat;

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";//http://inspiquo.rayvatapps.com/api/api.php?request=";1.0

    String JSON_URL = main_url+"getquotesbycatid&cid=";//"getquotesbycatid&category_id=";
    String JSON_IMG_URL = main_url+"getbackgrounds";

    String JSON_URL2=JSON_URL+newCat;
    String categoryname;

    private DrawerLayout mDrawerLayout;

    Intent intent;

    boolean doubleBackToExitPressedOnce = false;

    TextView quotesstatus;

    Mysql mySql;

    ArrayList<String> imageList;
    int filecount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageList = new ArrayList<String>();

        bundle= getIntent().getExtras();

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            newCat= 0;
        } else {
            newCat= bundle.getInt("position");
            categoryname= bundle.getString("catname");
            /*Log.e("srdfd", String.valueOf(newCat));
            Log.e("Categoryname ",categoryname);*/
        }

        JSON_URL+=newCat;
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        quotesstatus=(TextView)findViewById(R.id.tvstatus);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        quotesstatus.setTypeface(tf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(categoryname);
        //toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share app", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); /*Here's a Snackbar*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.

                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();

        adapter = new SubCategoryAdapter(this,albumList,newCat,categoryname);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();

        //adding the hero to herolist

       /* Quote a = new Quote("जय हनुमान ज्ञान गुन सागर ।\n" +
                "जय कपीस तिहुँ लोक उजागर ॥", "mitesh", "mitesh");

        albumList.add(a);*/
        //albumList.add(new hero("हिंदी","mitesh","mitesh"));

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...

                        albumList.clear();
                        imageList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);
            }
        });
    }

    private void checkconnection() {
        quotesstatus.setVisibility(View.GONE);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            loadAllQuoteimage();

            showBannerAddvertisement();
//            loadSubCategoryQuotes();

        }else{
            //no connection
            FetchImages();
            mySql = new Mysql(this);
//            imageList = mySql.getQuoteImage();
            albumList = mySql.getSubquotebyid(categoryname);
            if(albumList.size()==0)
            {
                quotesstatus.setVisibility(View.VISIBLE);

                Toast.makeText(this, "Please Go Online one time", Toast.LENGTH_SHORT).show();
            }
            adapter = new SubCategoryAdapter(this,albumList,newCat,categoryname,imageList);
            recyclerView.setAdapter(adapter);
        }
    }

    private void loadSubCategoryQuotes() {
        mySql = new Mysql(SubCategory.this);

        mySql.deleteAllSubCategoryquote(categoryname);
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        albumList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("response");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                Quote hero = new Quote(heroObject.getInt("Qid"),
                                        heroObject.getString("Quotecontent"),
                                        heroObject.getString("Lang"),
                                        heroObject.getString("Title"),
                                        heroObject.getString("Name"),
                                        heroObject.getString("Username"));


                                mySql.insertSubQuote(
                                        heroObject.getInt("Qid"),
                                        heroObject.getString("Quotecontent"),
                                        heroObject.getString("Title"),
                                        heroObject.getString("Name")

                                        /*,heroObject.getString("Author_username")*/
                                );

                                //adding the hero to herolist
                                albumList.add(hero);
                            }

                            //creating custom adapter object
                             adapter = new SubCategoryAdapter(SubCategory.this,albumList,newCat,categoryname,imageList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(SubCategory.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(SubCategory.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(SubCategory.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {

                            Toast.makeText(SubCategory.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        checkconnection();
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void loadAllQuoteimage() {

        mySql = new Mysql(SubCategory.this);

        mySql.deleteAllQuoteImage();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_IMG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);
                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("response");
                            ArrayList<String> image=new ArrayList<String>();

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                final String data=heroObject.getString("Image");

                                final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                final String fullpath=path+data;

                                image.add(data);

                            }

                            FetchImages();

                            if(image.size()!=filecount)
                            {
                                imageList.clear();
                                for(int i=0;i<image.size();i++)
                                {
                                    final String data=image.get(i);

                                    final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                    final String fullpath=path+data;

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try  {
                                                try {

                                                    //Your code goes here
                                                    URL url = new URL(fullpath);
                                                    HttpGet httpRequest = null;

                                                    httpRequest = new HttpGet(url.toURI());

                                                    HttpClient httpclient = new DefaultHttpClient();
                                                    HttpResponse response = (HttpResponse) httpclient
                                                            .execute(httpRequest);

                                                    HttpEntity entity = response.getEntity();
                                                    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                                                    InputStream input = b_entity.getContent();

                                                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                                                    BitmapDrawable background = new BitmapDrawable(bitmap);

                                                    saveImageToExternalStorage(bitmap,data);

                                               /* ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                //compress the image to jpg format
                                                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            *//*
                                                     * encode image to base64 so that it can be picked by saveImage.php file
                                                     * *//*
                                                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);
                                                //Log.e("image",encodeImage);
                                                imageList.add(encodeImage);

                                                mySql.insertQuoteImage(encodeImage);

                                                SharedPreferences imglist = getSharedPreferences("image", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = imglist.edit();
                                                Set<String> set = new HashSet<String>();
                                                set.addAll(imageList);
                                                editor.putStringSet("image", set);
                                                editor.commit();*/



                                                    //holder.lnimg.setBackgroundDrawable(background);

                                                } catch (URISyntaxException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();

                                                } catch (MalformedURLException e) {
                                                    Log.e("log", "bad url");
                                                } catch (IOException e) {
                                                    Log.e("log", "io error");
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });

                                    thread.start();
                                }

                            }

                            /*for (String member : imageList){
                                Log.i("--mainMember name: ", member);
                            }*/

                            loadSubCategoryQuotes();

                        } catch (JSONException e) {

                            //quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(volleyError));
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(SubCategory.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(SubCategory.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(SubCategory.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(SubCategory.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        checkconnection();
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void FetchImages() {

       /* String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Backgrounds");*/

        ArrayList<String> filenames = new ArrayList<String>();

        String path = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_PICTURES+File.separator+ "Inspiquo/.Inspiquo Backgrounds/";

        File directory = new File(path);
        if(!directory.exists())
        {
            directory.mkdir();
        }
        File[] files = directory.listFiles();

        try {
            for (int i = 0; i < files.length; i++)
            {
                String file_name = files[i].getName();
                // you can store name to arraylist and use it later
                filenames.add(file_name);
                filecount+=1;
                imageList.add(path+file_name);
            }
        }
        catch (NullPointerException e)
        {
            Log.e("null",e.getMessage().toString());
        }
//        return filenames;
    }

    private void showBannerAddvertisement() {
        AdView mAdMobAdView;

        mAdMobAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        adView.setAdSize(AdSize.BANNER);
        mAdMobAdView.loadAd(adRequest);

        mAdMobAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap, String data) {

        //imageList.add(encodeImage);

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/.Inspiquo Backgrounds");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
//        String fname = "Image-" + n + ".jpg";
        String fname = data;
        File file = new File(myDir, fname);

        String sdpath=file.getAbsolutePath();
//        Log.e("file",file.getAbsolutePath());
        //imageList.add(file.getAbsolutePath());
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
//                        Log.i("ExternalStorage", "Scanned " + path + ":");
//                        Log.i("ExternalStorage", "-> uri=" + uri);
                        imageList.add(path);
                    }
                });

//        Toast.makeText(this, "Saved in /Inspiquo/Inspiquo Images", Toast.LENGTH_SHORT).show();

    }

    public void displayAlert() {
        new AlertDialog.Builder(this).setMessage("Please Check Your Internet Connection and Try Again")
                .setTitle("Network Error")
                .setCancelable(false)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                finish();
                            }
                        })
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {

                            case R.id.nav_category:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,CategoriesActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,MainActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_author:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,AuthorActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_favourites:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,FavouritesActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_rate:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                Uri uri = Uri.parse("market://details?id=" + SubCategory.this.getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                // To count with Play market backstack, After pressing back button,
                                // to taken back to our application, we need to add following flags to intent.
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=" + SubCategory.this.getPackageName())));
                                }
                                return true;

                            case R.id.nav_about:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,AboutActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_privacy:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(SubCategory.this,PrivacyActivity.class);
                                startActivity(intent);
                                finish();
                                return true;
                        }
                        return false;
               /* menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                Toast.makeText(MainActivity.this, "Tghghlkh", Toast.LENGTH_SHORT).show();
                return true;*//**/
                    }
                });
    }
}
