/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rayvatapps.inspiquo;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.AlarmClock;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.karan.churi.PermissionManager.PermissionManager;
import com.rayvatapps.inspiquo.adapter.QuotesAdapter;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.model.Privacy;
import com.rayvatapps.inspiquo.model.Quote;
import com.rayvatapps.inspiquo.util.ForceUpdateAsync;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import static java.util.Calendar.MINUTE;

public class MainActivity extends AppCompatActivity {
    private Context mContext=MainActivity.this;

    private DrawerLayout mDrawerLayout;

    private RecyclerView recyclerView;
    private QuotesAdapter adapter;
    private List<Quote> albumList;
    public static ArrayList<String> imageList;
    private List<Privacy> privacyList;

    Intent intent;

    SwipeRefreshLayout sw_refresh;

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";

    // private static final String JSON_URL = "http://192.168.1.8:9001/api/api.php?request=getquotes";
    String JSON_URL = main_url+"getactivequotes";
    String JSON_IMG_URL = main_url+"getbackgrounds";
    String JSON_INSPIQUOAPPUPDATE = main_url+"appupdate";

    PermissionManager permissionManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    ArrayList<String> arryfavquoteid;

    boolean doubleBackToExitPressedOnce = false;
    TextView quotesstatus;

    Mysql mySql;

    String sdpath="";
    int filecount=0;

    int refreshAdd=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        forceUpdate();

        getpermissions();

        arryfavquoteid = new ArrayList<String>();
        imageList = new ArrayList<String>();

        quotesstatus=(TextView)findViewById(R.id.tvstatus);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        quotesstatus.setTypeface(tf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share app", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); /*Here's a Snackbar*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());

                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        forceUpdate();

                        albumList.clear();
                        imageList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        //adapter = new QuotesAdapter(getApplicationContext(),albumList);

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//        recyclerView.setItemViewCacheSize(9);

        albumList = new ArrayList<>();
        adapter = new QuotesAdapter(this,albumList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        /*AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(mContext.getString(R.string.banner_footer));*/

        /*String android_id = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);

        Log.e("Device id",android_id);
        mAdMobAdView = (AdView) findViewById(R.id.admob_adview);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(android_id)// Add your real device id here //demo 4DD0986B8BB49093161F4F00CF61B887
                .build();
        mAdMobAdView.loadAd(adRequest);
*/
       checkconnection();

    }

    private void checkconnection() {

        quotesstatus.setVisibility(View.GONE);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            imageList.clear();
            loadAllQuoteimage();

            if(refreshAdd==0)
            {
                refreshAdd=1;
                showBannerAddvertisement();
            }

            Toast.makeText(mContext, "Tap to change background", Toast.LENGTH_SHORT).show();

            //connection is avlilable
        }else{
            //no connection
            mySql = new Mysql(this);
            albumList = mySql.getHomequote();
            FetchImages();
//            imageList = mySql.getQuoteImage();
            if(albumList.size()==0)
            {
                quotesstatus.setVisibility(View.VISIBLE);

                Toast.makeText(this, "Please Go Online one time", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(mContext, "Tap to change background", Toast.LENGTH_SHORT).show();
            }
            adapter = new QuotesAdapter(this,albumList,imageList);
            recyclerView.setAdapter(adapter);

            //displayAlert();
            //Toast toast = Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG);toast.show();
        }
    }

    private void showBannerAddvertisement() {
        AdView mAdMobAdView;

        mAdMobAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        adView.setAdSize(AdSize.BANNER);
        mAdMobAdView.loadAd(adRequest);

        mAdMobAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void loadAllQuotejsondata() {

        mySql = new Mysql(MainActivity.this);

        mySql.deleteAllHomequote();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);
//        albumList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);
                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("response");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                Quote hero = new Quote(heroObject.getInt("Qid"),
                                        heroObject.getString("Quotecontent"),
                                        heroObject.getString("Lang"),
                                        heroObject.getString("Name"),
                                        heroObject.getString("Username"));

                                mySql.insertHomeQuote(
                                        heroObject.getInt("Qid"),
                                        heroObject.getString("Quotecontent"),
                                        heroObject.getString("Name")
                                        /*,heroObject.getString("Author_username")*/
                                );

                                //adding the hero to herolist
                                albumList.add(hero);
                            }

                            //FetchImages();

                            /*for (String member : imageList){
                                Log.i("2mainMember name: ", member);
                            }*/
                            //creating custom adapter object
                            adapter = new QuotesAdapter(MainActivity.this,albumList,imageList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {

                            quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(volleyError));
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(MainActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(MainActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(MainActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
//                        checkconnection();
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void loadAllQuoteimage() {

        mySql = new Mysql(MainActivity.this);

        mySql.deleteAllQuoteImage();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //getting the progressbar

        //making the progressbar visible
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_IMG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);
                            JSONArray heroArray = obj.getJSONArray("response");

                            ArrayList<String> image=new ArrayList<String>();
                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                final String data=heroObject.getString("Image");
                                image.add(data);

                                /*final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                final String fullpath=path+data;

                                Thread thread = new Thread(new Runnable() {

                                    @Override
                                    public void run() {
                                        try  {
                                            try {

                                                //Your code goes here
                                                URL url = new URL(fullpath);
                                                HttpGet httpRequest = null;

                                                httpRequest = new HttpGet(url.toURI());

                                                HttpClient httpclient = new DefaultHttpClient();
                                                HttpResponse response = (HttpResponse) httpclient
                                                        .execute(httpRequest);

                                                HttpEntity entity = response.getEntity();
                                                BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                                                InputStream input = b_entity.getContent();

                                                Bitmap bitmap = BitmapFactory.decodeStream(input);
                                                BitmapDrawable background = new BitmapDrawable(bitmap);

                                                saveImageToExternalStorage(bitmap,data);

                                               *//* ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                //compress the image to jpg format
                                                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            *//**//*
            * encode image to base64 so that it can be picked by saveImage.php file
            * *//**//*
                                                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);
                                                //Log.e("image",encodeImage);
                                                imageList.add(encodeImage);

                                                mySql.insertQuoteImage(encodeImage);

                                                SharedPreferences imglist = getSharedPreferences("image", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = imglist.edit();
                                                Set<String> set = new HashSet<String>();
                                                set.addAll(imageList);
                                                editor.putStringSet("image", set);
                                                editor.commit();*//*



                                                //holder.lnimg.setBackgroundDrawable(background);

                                            } catch (URISyntaxException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();

                                            } catch (MalformedURLException e) {
                                                Log.e("log", "bad url");
                                            } catch (IOException e) {
                                                Log.e("log", "io error");
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                                thread.start();*/

                            }
                                    FetchImages();

                            if(image.size()!=filecount)
                            {
                                imageList.clear();
                                for(int i=0;i<image.size();i++)
                                {
                                    final String data=image.get(i);

                                    final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                    final String fullpath=path+data;

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                                try {

                                                    //Your code goes here
                                                    URL url = new URL(fullpath);
                                                    HttpGet httpRequest = null;

                                                    httpRequest = new HttpGet(url.toURI());

                                                    HttpClient httpclient = new DefaultHttpClient();
                                                    HttpResponse response = (HttpResponse) httpclient
                                                            .execute(httpRequest);

                                                    HttpEntity entity = response.getEntity();
                                                    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                                                    InputStream input = b_entity.getContent();

                                                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                                                    BitmapDrawable background = new BitmapDrawable(bitmap);

                                                    saveImageToExternalStorage(bitmap,data);

                                               /* ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                //compress the image to jpg format
                                                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            *//*
                                                     * encode image to base64 so that it can be picked by saveImage.php file
                                                     * *//*
                                                String encodeImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);
                                                //Log.e("image",encodeImage);
                                                imageList.add(encodeImage);

                                                mySql.insertQuoteImage(encodeImage);

                                                SharedPreferences imglist = getSharedPreferences("image", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = imglist.edit();
                                                Set<String> set = new HashSet<String>();
                                                set.addAll(imageList);
                                                editor.putStringSet("image", set);
                                                editor.commit();*/



                                                    //holder.lnimg.setBackgroundDrawable(background);

                                                } catch (URISyntaxException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();

                                                } catch (MalformedURLException e) {
                                                    Log.e("log", "bad url");
                                                } catch (IOException e) {
                                                    Log.e("log", "io error");
                                                }

                                        }
                                    });

                                    thread.start();
                                }

                            }

                            loadAllQuotejsondata();

                        } catch (JSONException e) {

                            //quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(volleyError));
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(MainActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(MainActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(MainActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
//                        checkconnection();
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap, String data) {

        //imageList.add(encodeImage);

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/.Inspiquo Backgrounds");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
//        String fname = "Image-" + n + ".jpg";
        String fname = data;
        File file = new File(myDir, fname);

        sdpath=file.getAbsolutePath();
//        Log.e("file",file.getAbsolutePath());
        //imageList.add(file.getAbsolutePath());
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
//                        Log.i("ExternalStorage", "Scanned " + path + ":");
//                        Log.i("ExternalStorage", "-> uri=" + uri);
                        imageList.add(path);
                    }
                });

//        Toast.makeText(this, "Saved in /Inspiquo/Inspiquo Images", Toast.LENGTH_SHORT).show();

    }

    private void FetchImages() {

       /* String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Backgrounds");*/

        ArrayList<String> filenames = new ArrayList<String>();

        String path = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_PICTURES+File.separator+ "Inspiquo/.Inspiquo Backgrounds/";

        File directory = new File(path);
        if(!directory.exists())
        {
            directory.mkdir();
        }
        File[] files = directory.listFiles();

        try
        {
            for (int i = 0; i < files.length; i++)
            {

                String file_name = files[i].getName();
                // you can store name to arraylist and use it later
                filenames.add(file_name);
                filecount+=1;
                imageList.add(path+file_name);
            }
        }
        catch (NullPointerException e)
        {
            Log.e("null",e.getMessage().toString());
        }

//        return filenames;
    }

    public void displayAlert() {
        new AlertDialog.Builder(this).setMessage("Please Check Your Internet Connection and Try Again")
                .setTitle("Network Error")
                .setCancelable(false)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                finish();
                            }
                        })
                .show();
    }

    private void getpermissions() {

        permissionManager=new PermissionManager() {

            @Override
            public List<String> setPermission() {
                // If You Don't want to check permission automatically and check your own custom permission
                // Use super.setPermission(); or Don't override this method if not in use
                List<String> customPermission=new ArrayList<>();
                customPermission.add(Manifest.permission.INTERNET);
//                customPermission.add(Manifest.permission.ACCESS_WIFI_STATE);
                customPermission.add(Manifest.permission.ACCESS_NETWORK_STATE);
                customPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                customPermission.add(Manifest.permission.SET_WALLPAPER);
                return customPermission;
            }
        };

        //To initiate checking permission
        permissionManager.checkAndRequestPermissions(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        permissionManager.checkResult(requestCode,permissions,grantResults);

        ArrayList<String> granted=permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied=permissionManager.getStatus().get(0).denied;

        for(String item:granted)
            Log.e("granted",item);

        for(String item:denied)
            Log.e("granted",item);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        //startTrackerService();
                        Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
        }

    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sample_actions, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

           /* case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/

        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.getMenu().getItem(0).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {

                    case R.id.nav_category:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(MainActivity.this,CategoriesActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_home:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        /* intent = new Intent(MainActivity.this,CategoriesActivity.class);
                        startActivity(intent);
                        finish();*/
                        return true;

                    case R.id.nav_author:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(MainActivity.this,AuthorActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_favourites:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(MainActivity.this,FavouritesActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_rate:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        Uri uri = Uri.parse("market://details?id=" + MainActivity.this.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    // To count with Play market backstack, After pressing back button,
                    // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName())));
                        }
                        return true;

                    case R.id.nav_about:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(MainActivity.this,AboutActivity.class);
                        startActivity(intent);
                        finish();
                        return true;

                    case R.id.nav_privacy:
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        intent = new Intent(MainActivity.this,PrivacyActivity.class);
                        startActivity(intent);
                        finish();
                        return true;
                }
                return false;
               /* menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                Toast.makeText(MainActivity.this, "Tghghlkh", Toast.LENGTH_SHORT).show();
                return true;*//**/
            }
        });
    }
    // check version on play store and force update
    public void forceUpdate(){
        /*PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion,MainActivity.this).execute();*/

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, JSON_INSPIQUOAPPUPDATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONArray heroArray = obj.getJSONArray("response");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //heroObject.getString("Site_id")
                                //creating a hero object and giving them the values from json object
                                int id=heroObject.getInt("id");
                                String version=heroObject.getString("app_version");
                                String froce_update= heroObject.getString("force_upgrade");
                                String recommend_update= heroObject.getString("recommend_upgrade");

//                                Log.e("id"+id+version,froce_update.toString()+recommend_update);
                                PackageManager packageManager = getPackageManager();
                                PackageInfo packageInfo = null;
                                try {
                                    packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }
                                String currentVersion = packageInfo.versionName;
                                if(!currentVersion.equals(version))
                                {
                                    ForceUpdateAsync.showForceUpdateDialog(mContext,version,froce_update,recommend_update);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
