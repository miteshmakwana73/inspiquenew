package com.rayvatapps.inspiquo;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rayvatapps.inspiquo.adapter.FavouriteAdapter;
import com.rayvatapps.inspiquo.adapter.QuotesAdapter;
import com.rayvatapps.inspiquo.db.Mysql;
import com.rayvatapps.inspiquo.model.Quote;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FavouritesActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private RecyclerView recyclerView;
    FavouriteAdapter adapter;
    private List<Quote> albumList;

    Intent intent;

    SwipeRefreshLayout sw_refresh;

    Mysql mySql;

    boolean doubleBackToExitPressedOnce = false;
    TextView quotesstatus;

    ArrayList<String> imageList;

    String main_url="http://inspiquo.rayvatapps.com/v2/api.php?request=";
    String JSON_IMG_URL = main_url+"getbackgrounds";

    ProgressBar progressBar;
    private ProgressDialog progressDialog;

//    LinearLayout click;
    int filecount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressDialog = new ProgressDialog(this);

        imageList = new ArrayList<String>();

        quotesstatus=(TextView)findViewById(R.id.tvstatus);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        quotesstatus.setTypeface(tf);

        //click=(LinearLayout)findViewById(R.id.lnquote);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Favourites");
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share app", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); /*Here's a Snackbar*/

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.

                share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing) + " :- https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());

                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();
                        imageList.clear();

                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList= new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        checkconnection();
    }

    private void checkconnection() {

        quotesstatus.setVisibility(View.GONE);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
//            loadAllQuoteimage();
            UpdateAdaptrer2();

            showBannerAddvertisement();
            //connection is avlilable
        }else{
            //no connection
            FetchImages();
            UpdateAdaptrer();
        }
    }

    private void UpdateAdaptrer() {
        quotesstatus.setVisibility(View.GONE);
        albumList.clear();
        mySql = new Mysql(this);
//        imageList = mySql.getQuoteImage();
        albumList = mySql.getDataFromDB();
        if(albumList.size()==0)
        {
            quotesstatus.setVisibility(View.VISIBLE);

            Toast.makeText(this, "favourite list is Empty", Toast.LENGTH_SHORT).show();
        }

        adapter = new FavouriteAdapter(this,albumList,imageList);
        recyclerView.setAdapter(adapter);

    }

    private void UpdateAdaptrer2() {
//        FetchImages();
        progressDialog.hide();
        quotesstatus.setVisibility(View.GONE);
        albumList.clear();
        mySql = new Mysql(this);
        albumList = mySql.getDataFromDB();
        if(albumList.size()==0)
        {
            quotesstatus.setVisibility(View.VISIBLE);

            Toast.makeText(this, "favourite list is Empty", Toast.LENGTH_SHORT).show();
        }

        //FetchImages();
        adapter = new FavouriteAdapter(this,albumList,MainActivity.imageList);
        recyclerView.setAdapter(adapter);

    }

    private void showBannerAddvertisement() {
        AdView mAdMobAdView;

        mAdMobAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        adView.setAdSize(AdSize.BANNER);
        mAdMobAdView.loadAd(adRequest);

        mAdMobAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void loadAllQuoteimage() {

        mySql = new Mysql(FavouritesActivity.this);

        mySql.deleteAllQuoteImage();
        //getting the progressbar

        //making the progressbar visible
        /*progressDialog.setMessage("Please Wait...");
        progressDialog.show();*/
        progressBar.setVisibility(View.VISIBLE);

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_IMG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //Log.e("Data",response);
                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("response");
                            ArrayList<String> image=new ArrayList<String>();

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                final String data=heroObject.getString("Image");

                                final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                final String fullpath=path+data;

                                image.add(data);

                                //progressBar.setVisibility(View.VISIBLE);


                            }

                            FetchImages();

                            if(image.size()!=filecount)
                            {
                                imageList.clear();
                                for(int i=0;i<image.size();i++)
                                {
                                    final String data=image.get(i);

                                    final String path="http://inspiquo.rayvatapps.com/web/assets/images/backgrounds/";

                                    final String fullpath=path+data;

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try  {
                                                try {

                                                    //Your code goes here
                                                    URL url = new URL(fullpath);
                                                    HttpGet httpRequest = null;

                                                    httpRequest = new HttpGet(url.toURI());

                                                    HttpClient httpclient = new DefaultHttpClient();
                                                    HttpResponse response = (HttpResponse) httpclient
                                                            .execute(httpRequest);

                                                    HttpEntity entity = response.getEntity();
                                                    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                                                    InputStream input = b_entity.getContent();

                                                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                                                    BitmapDrawable background = new BitmapDrawable(bitmap);

                                                    saveImageToExternalStorage(bitmap,data);

                                                } catch (URISyntaxException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();

                                                } catch (MalformedURLException e) {
                                                    Log.e("log", "bad url");
                                                } catch (IOException e) {
                                                    Log.e("log", "io error");
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });

                                    thread.start();
                                }

                            }

                            //progressBar.setVisibility(View.INVISIBLE);

                            /*for (String member : imageList){
                                Log.i("--mainMember name: ", member);
                            }*/

                            UpdateAdaptrer2();

                        } catch (JSONException e) {

                            //quotesstatus.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        //Log.e("error", String.valueOf(volleyError));
                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(FavouritesActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(FavouritesActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                            UpdateAdaptrer();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(FavouritesActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(FavouritesActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        checkconnection();
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void saveImageToExternalStorage(Bitmap finalBitmap, String data) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Backgrounds");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
//        String fname = "Image-" + n + ".jpg";
        String fname = data;
        File file = new File(myDir, fname);

        Log.e("file",file.getAbsolutePath());
        imageList.add(file.getAbsolutePath());
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                       /* Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);*/
                    }
                });

        //recyclerView.setEnabled(true);

//        Toast.makeText(this, "Saved in /Inspiquo/Inspiquo Images", Toast.LENGTH_SHORT).show();

    }

    private void FetchImages() {

       /* String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Inspiquo/Inspiquo Backgrounds");*/

        ArrayList<String> filenames = new ArrayList<String>();

        String path = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_PICTURES+File.separator+ "Inspiquo/.Inspiquo Backgrounds/";

        File directory = new File(path);
        if(!directory.exists())
        {
            directory.mkdir();
        }
        File[] files = directory.listFiles();

        try {
            for (int i = 0; i < files.length; i++)
            {

                String file_name = files[i].getName();
                // you can store name to arraylist and use it later
                filenames.add(file_name);
                filecount+=1;
                imageList.add(path+file_name);
            }
        }
        catch (NullPointerException e)
        {
            Log.e("null",e.getMessage().toString());
        }

//        return filenames;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            /*case R.id.action_settings:
                Toast.makeText(MainActivity.this, "Settng", Toast.LENGTH_SHORT).show();
                return true;*/

        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.getMenu().getItem(3).setChecked(true);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {

                            case R.id.nav_category:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(FavouritesActivity.this,CategoriesActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_home:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                 intent = new Intent(FavouritesActivity.this,MainActivity.class);

                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_author:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(FavouritesActivity.this,AuthorActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_favourites:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                /*intent = new Intent(FavouritesActivity.this,FavouritesActivity.class);
                                startActivity(intent);
                                finish();*/
                                return true;

                            case R.id.nav_rate:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                Uri uri = Uri.parse("market://details?id=" + FavouritesActivity.this.getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                // To count with Play market backstack, After pressing back button,
                                // to taken back to our application, we need to add following flags to intent.
                                //Log.e("pac "+getPackageName(),"pacthis "+FavouritesActivity.this.getPackageName());

                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=" + FavouritesActivity.this.getPackageName())));

                                }
                                return true;

                            case R.id.nav_about:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(FavouritesActivity.this,AboutActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                            case R.id.nav_privacy:
                                menuItem.setChecked(true);
                                mDrawerLayout.closeDrawers();
                                intent = new Intent(FavouritesActivity.this,PrivacyActivity.class);
                                startActivity(intent);
                                finish();
                                return true;

                        }
                        return false;
               /* menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                Toast.makeText(MainActivity.this, "Tghghlkh", Toast.LENGTH_SHORT).show();
                return true;*//**/
                    }
                });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
